import cv2
import sys

import numpy as np
image = np.zeros((512,512,3), np.uint8)

# Get user supplied image
#imagePath = sys.argv[1]

# read the image
#image = cv2.imread(imagePath)

cv2.rectangle(image,(384,0),(510,128),(0,255,0),3)

# Draw a diagonal blue line with thickness of 5 px
#cv2.line(image,(0,0),(511,511),(255,0,0),5)
#start,end,width,hight,color,linewidth

#cv2.circle(image,(447,63), 63, (0,0,255), -1)
#start,end,diameter,color,linewidth

#cv2.ellipse(image,(256,256),(100,50),100,0,350,(0,255,0),-1)
#start,end,width,hight,rotation,start,end,color,linewidth

#text
#font = cv2.FONT_HERSHEY_SIMPLEX
#cv2.putText(image,'OpenCV',(10,100), font, 4,(255,255,255),2,cv2.LINE_AA)

#show image
cv2.imshow("Import image", image)

#wait for a quit key
cv2.waitKey(0)