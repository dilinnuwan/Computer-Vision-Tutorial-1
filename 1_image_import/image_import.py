import cv2
import sys

# Get user supplied image
imagePath = sys.argv[1]

# read the image
image = cv2.imread(imagePath)

#show image
cv2.imshow("Import image", image)

#wait for a quit key
cv2.waitKey(0)